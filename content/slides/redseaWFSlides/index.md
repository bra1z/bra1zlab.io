+++

title = " ❄ redseaWF slides"
description=" ☀ GUI Front-end for redsea program."

type="slide"

theme = "simple"
[revealOptions]
transition= 'none'
controls= true
progress= true
history= true
center= true

+++

![redseaWF](screenshot.png)

---

[<<](../)

<font color='green'>redseaWF</font> is a GUI Front-end for [redsea](https://github.com/windytan/redsea) program.

[<<](../)



