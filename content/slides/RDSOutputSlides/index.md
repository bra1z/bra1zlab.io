+++

title = " ❄ RDSOutput slides"
description=" ☀ RDS plugin for SDR# program."

type="slide"

theme = "simple"
[revealOptions]
transition= 'none'
controls= true
progress= true
history= true
center= true

+++

![RDSOutput](rdsoutput.gif)

---

[<<](../)

<font color='red'>RDSOutput</font> is a RDS plugin for [“SDR#”](https://airspy.com/download/) program.

[<<](../)






