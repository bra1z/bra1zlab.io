+++
title   = "Location Tables"
description = "Location Tables for redsea program."
weight  = "42"

pre ="<i class='fa fa-edit' >&nbsp;&nbsp;</i> "
+++

Location Tables
===============

Location Tables to use TMC with redsea program

[https://gitlab.com/bra1z/LocationTables](https://gitlab.com/bra1z/LocationTables)

Check README.DAT to change ISO-8859-1 or ISO-8859-15

