+++
title   = "RDSOutput"
weight  = "42"
description = "RDS plugin for SDR# program."
pre ="<i class='fa fa-edit' >&nbsp;&nbsp;</i> "
+++

# RDSOutput

**SDR# plugin for "RDS Spy" program.**

[https://gitlab.com/bra1z/RDSOutput](https://gitlab.com/bra1z/RDSOutput)

Maybe someone want to try my plugin for SDR#. The connection with RDSSpy is through socket TCP/IP ( No virtual audio cable). You have to add manually the line:

	<add key="RDS Output" value="SDRSharp.RDSOutput.RDSOutput,SDRSharp.RDSOutput" />

in **Plugins.xml** file in your SDR# program directory and configure RDSSpy for TCP/IP connection  with  IP host:  _localhost_ and _port_:  23

Also you have to copy the file **SDRSharp.RDSOutput.dll** in your SDR# program directory.
You can get the file **SDRSharp.RDSOutput.dll** from:

[https://gitlab.com/bra1z/RDSOutput](https://gitlab.com/bra1z/RDSOutput)

No need to compile it, get the file from _RDSOutput/RDSOutput/bin/Debug/_  directory.

Now with RFtap output:
[https://rftap.github.io/blog/2016/08/27/decoding-rds-with-rftap.html](https://rftap.github.io/blog/2016/08/27/decoding-rds-with-rftap.html)

![demo](rdsoutput.gif?classes=border,shadow)

![demo](v1.0.0.1491.png?classes=border,shadow)

