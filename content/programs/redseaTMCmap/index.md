+++
title   = "redseaTMCmap"
description = "RDS-TMC map visualization tool for Windows using redsea."
weight  = "41"
pre ="<i class='fa fa-edit' >&nbsp;&nbsp;</i> "
+++

# redseaTMCmap

**RDS-TMC map visualization tool for Windows**

[https://gitlab.com/bra1z/redseaTMCmap](https://gitlab.com/bra1z/redseaTMCmap)

[![Build status](https://ci.appveyor.com/api/projects/status/34s5akex0scjsp7t/branch/master?svg=true)](https://ci.appveyor.com/project/bra1z/redseatmcmap/branch/master)

This program uses [https://github.com/windytan/redsea](https://github.com/windytan/redsea) to get the TMC traffic data.

The map library used is [GMap.NET](https://github.com/radioman/greatmaps)

You have to edit the **run.cmd** file to put the correct frequency and gain parameters. 

	rtl_fm -M fm -l 0 -A std -p 0 -s 171k -g 10 -F 9 -f 93.2M | redsea -l TMC

Also put in TMC folder the correct location tables files.  See [https://gitlab.com/bra1z/LocationTables](https://gitlab.com/bra1z/LocationTables) to find the correct ones.

![demo](TMCmap.png)


