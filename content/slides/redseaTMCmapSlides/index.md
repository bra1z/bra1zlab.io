+++

title = " ❄ redseaTMCmap slides"
description=" ☀ RDS-TMC map visualization tool for Windows using redsea."

type="slide"

theme = "simple"
[revealOptions]
transition= 'none'
controls= true
progress= true
history= true
center= true

+++

![TMCmap](TMCmap.png)

---

[<<](../)

<font color='green'>redseaTMCmap</font> is a RDS-TMC map visualization tool for Windows using [redsea](https://github.com/windytan/redsea).

[<<](../)
