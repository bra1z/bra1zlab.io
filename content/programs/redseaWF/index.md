+++
title   = "redseaWF"
description = "GUI Front-end for redsea program."
weight  = "43"
pre ="<i class='fa fa-edit' >&nbsp;&nbsp;</i> "
+++

## redseaWF

[https://gitlab.com/bra1z/redseaWF](https://gitlab.com/bra1z/redseaWF)

[![Build status](https://ci.appveyor.com/api/projects/status/1jx2vxvclglfeda5?svg=true)](https://ci.appveyor.com/project/bra1z/redseawf)

**GUI Front-end for redsea.**

> Tested only for Windows Seven 32 bits.





{{<mermaid align="left">}}
graph LR;
    A(rtl_fm) -->|cmd-pipe| B(redsea)
    B -->|cmd-pipe| C(redseaWF)

{{< /mermaid >}}






![demo](screenshot.png?classes=border,shadow)

