+++
description = "pied de page"

hidden = true
+++

<div class="pied-de-page">
<a href="https://bra1z.gitlab.io" target="_blank">

<img src="/images/bra1zdocs.png" alt="bra1z documents"></a>

<p><a href="https://bra1z.gitlab.io" target="_blank">bra1z Gitlab documents site</a></p>
</div>
